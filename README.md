EddyIMG by Der-Eddy
=====================

Ich weiß das der Code weder sonderlich sauber ist noch ich Lust habe alles zu übersetzen. Hoffe aber trotzdem das irgendwer einen Nutzen davon tragen kann
Hauptsätzlich ist das Repo jedoch da damit ich ihren Fortschritt dokumentieren kann


----------

Installation:
-------------

Für so ein kleines Projekt nutze ich kein [PHP Composer][1] mehr. Einfach downloaden und in euren PHP Server packen.

----------

Was es zu bieten hat:
---------------------

 - Flat Files statt Datenbankverbindungen
 - Schöne Auflistung der Bilder
 - Implentierung des Autorennamens in den Dateinamen der Bilder
 - Schönes UI


----------

 
Was noch kommen wird (oder auch nicht):
---------------------

 - Cookie Sessions
 - Anime Chicks
 - Schöneres Upload Modal
 - Schönere Ausgabe der PHP Verarbeitung (z.B. "Login granted")
 - Schönere PHP Funktionen oder sogar etwas OOP (Oben-Ohne-Programmierung)






  [1]: https://getcomposer.org/
  [2]: http://semantic-ui.com/
  [3]: https://packagist.org/
